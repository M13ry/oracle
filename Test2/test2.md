### 班级：软件工程1班     学号：202010111512     姓名：牟梦元

# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

- 第1步：以system登录到pdborcl，创建角色ruoJ_role和用户LUOJIA，并授权和分配空间：

- 第2步：新用户LUOJIA连接到pdborcl，创建表customers和视图customers_view

![](img2/img1.png)

- 第3步：用户hr连接到pdborcl，查询LUOJIA授予它的视图customers_view

![](img2/img2.png)

## 概要文件设置,用户最多登录时最多只能错误3次

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

![](img2/img3.png)

## 查看数据库的使用情况

查看表空间的数据库文件，以及每个文件的磁盘占用情况。

![](img2/img4.png)

## 实验结束删除用户和角色

![](img2/img5.png)

## 实验参考

- SQL-DEVELOPER修改用户的操作界面：
![](img2/img6.png)

- sqldeveloper授权对象的操作界面：
![](img2/img7.png)
