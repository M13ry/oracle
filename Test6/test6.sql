CREATE TABLESPACE sales
DATAFILE 'sales.dbf' SIZE 100M;

CREATE TABLESPACE inventory
DATAFILE 'inventory.dbf' SIZE 100M;

CREATE TABLE products (
  product_id NUMBER PRIMARY KEY,
  product_name VARCHAR2(100) NOT NULL,
  product_description VARCHAR2(500),
  product_price NUMBER(10,2) NOT NULL
) TABLESPACE sales;

CREATE TABLE customers (
  customer_id NUMBER PRIMARY KEY,
  customer_name VARCHAR2(100) NOT NULL,
  customer_address VARCHAR2(500),
  customer_phone VARCHAR2(20)
) TABLESPACE sales;

CREATE TABLE orders (
  order_id NUMBER PRIMARY KEY,
  customer_id NUMBER NOT NULL,
  order_date DATE NOT NULL,
  order_total NUMBER(10,2) NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
) TABLESPACE sales;

CREATE TABLE order_items (
  order_id NUMBER NOT NULL,
  product_id NUMBER NOT NULL,
  quantity NUMBER NOT NULL,
  unit_price NUMBER(10,2) NOT NULL,
  FOREIGN KEY (order_id) REFERENCES orders(order_id),
  FOREIGN KEY (product_id) REFERENCES products(product_id)
) TABLESPACE sales;

CREATE USER sales_manager IDENTIFIED BY password
DEFAULT TABLESPACE sales
TEMPORARY TABLESPACE temp;

GRANT CONNECT, RESOURCE TO sales_manager;
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO sales_manager;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales.products TO sales_manager;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales.customers TO sales_manager;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales.orders TO sales_manager;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales.order_items TO sales_manager;

CREATE USER inventory_manager IDENTIFIED BY password
DEFAULT TABLESPACE inventory
TEMPORARY TABLESPACE temp;

GRANT CONNECT, RESOURCE TO inventory_manager;
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO inventory_manager;
GRANT SELECT, INSERT, UPDATE, DELETE ON inventory.products TO inventory_manager;
GRANT SELECT ON sales.customers TO inventory_manager;
CREATE OR REPLACE PACKAGE sales_pkg AS
  PROCEDURE create_order (p_customer_id IN NUMBER, p_order_date IN DATE);
  PROCEDURE add_order_item (p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER);
  FUNCTION get_order_total (p_order_id IN NUMBER) RETURN NUMBER;
END sales_pkg;
/

CREATE OR REPLACE PACKAGE BODY sales_pkg AS
  PROCEDURE create_order (p_customer_id IN NUMBER, p_order_date IN DATE) IS
  BEGIN
    INSERT INTO orders (order_id, customer_id, order_date, order_total)
    VALUES (order_seq.NEXTVAL, p_customer_id, p_order_date, 0);
  END create_order;

  PROCEDURE add_order_item (p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER) IS
    v_unit_price NUMBER;
  BEGIN
    SELECT product_price INTO v_unit_price FROM products WHERE product_id = p_product_id;
    INSERT INTO order_items (order_id, product_id, quantity, unit_price)
    VALUES (p_order_id, p_product_id, p_quantity, v_unit_price);
    UPDATE orders SET order_total = order_total + v_unit_price * p_quantity WHERE order_id = p_order_id;
  END add_order_item;

  FUNCTION get_order_total (p_order_id IN NUMBER) RETURN NUMBER IS
    v_order_total NUMBER;
  BEGIN
    SELECT order_total INTO v_order_total FROM orders WHERE order_id = p_order_id;
    RETURN v_order_total;
  END get_order_total;
END sales_pkg;
/