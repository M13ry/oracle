﻿# 牟梦元	202010111512	20软工1班

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 设计方案

### 1.表及表空间设计方案：

- 表空间1：`sales`，用于存储销售相关的数据。

- 表空间2：`inventory`，用于存储库存相关的数据。

- 表1：`products`，存储商品信息，包括商品ID、名称、描述、价格等。

- 表2：`customers`，存储客户信息，包括客户ID、姓名、地址、电话等。

- 表3：`orders`，存储订单信息，包括订单ID、客户ID、订单日期、总金额等。

- 表4：`order_items`，存储订单明细信息，包括订单ID、商品ID、数量、单价等。

- ```
  -- 创建表空间
  CREATE TABLESPACE sales
  DATAFILE 'sales.dbf' SIZE 100M;
  
  CREATE TABLESPACE inventory
  DATAFILE 'inventory.dbf' SIZE 100M;
  
  -- 创建表
  CREATE TABLE products (
    product_id NUMBER PRIMARY KEY,
    product_name VARCHAR2(100) NOT NULL,
    product_description VARCHAR2(500),
    product_price NUMBER(10,2) NOT NULL
  ) TABLESPACE sales;
  
  CREATE TABLE customers (
    customer_id NUMBER PRIMARY KEY,
    customer_name VARCHAR2(100) NOT NULL,
    customer_address VARCHAR2(500),
    customer_phone VARCHAR2(20)
  ) TABLESPACE sales;
  
  CREATE TABLE orders (
    order_id NUMBER PRIMARY KEY,
    customer_id NUMBER NOT NULL,
    order_date DATE NOT NULL,
    order_total NUMBER(10,2) NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
  ) TABLESPACE sales;
  
  CREATE TABLE order_items (
    order_id NUMBER NOT NULL,
    product_id NUMBER NOT NULL,
    quantity NUMBER NOT NULL,
    unit_price NUMBER(10,2) NOT NULL,
    FOREIGN KEY (order_id) REFERENCES orders(order_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id)
  ) TABLESPACE sales;
  ```

  ![](./img6/1.png)

### 2.权限及用户分配方案：

- 用户1：`sales_manager`，负责销售管理，具有对`sales`表空间中所有表的查询、插入、更新和删除权限。

- 用户2：`inventory_manager`，负责库存管理，具有对`inventory`表空间中所有表的查询、插入、更新和删除权限。

- ```
  -- 创建用户并分配权限
  CREATE USER sales_manager IDENTIFIED BY password
  DEFAULT TABLESPACE sales
  TEMPORARY TABLESPACE temp;
  
  GRANT CONNECT, RESOURCE TO sales_manager;
  GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO sales_manager;
  GRANT SELECT, INSERT, UPDATE, DELETE ON sales.products TO sales_manager;
  GRANT SELECT, INSERT, UPDATE, DELETE ON sales.customers TO sales_manager;
  GRANT SELECT, INSERT, UPDATE, DELETE ON sales.orders TO sales_manager;
  GRANT SELECT, INSERT, UPDATE, DELETE ON sales.order_items TO sales_manager;
  
  CREATE USER inventory_manager IDENTIFIED BY password
  DEFAULT TABLESPACE inventory
  TEMPORARY TABLESPACE temp;
  
  GRANT CONNECT, RESOURCE TO inventory_manager;
  GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO inventory_manager;
  GRANT SELECT, INSERT, UPDATE, DELETE ON inventory.products TO inventory_manager;
  GRANT SELECT ON sales.customers TO inventory_manager;
  ```

  ![](./img6/2.png)

### 3.程序包设计：

- 程序包名称：`sales_pkg`

- 存储过程1：`create_order`，用于创建新订单。

- 存储过程2：`add_order_item`，用于向订单中添加商品。

- 函数1：`get_order_total`，用于计算订单总金额。

- ```
  CREATE OR REPLACE PACKAGE sales_pkg AS
    -- 定义存储过程和函数的原型
    PROCEDURE create_order (p_customer_id IN NUMBER, p_order_date IN DATE);
    PROCEDURE add_order_item (p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER);
    FUNCTION get_order_total (p_order_id IN NUMBER) RETURN NUMBER;
  END sales_pkg;
  /
  
  CREATE OR REPLACE PACKAGE BODY sales_pkg AS
    -- 定义存储过程和函数的实现
    PROCEDURE create_order (p_customer_id IN NUMBER, p_order_date IN DATE) IS
    BEGIN
      INSERT INTO orders (order_id, customer_id, order_date, order_total)
      VALUES (order_seq.NEXTVAL, p_customer_id, p_order_date, 0);
    END create_order;
  
    PROCEDURE add_order_item (p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER) IS
      v_unit_price NUMBER;
    BEGIN
      SELECT product_price INTO v_unit_price FROM products WHERE product_id = p_product_id;
      INSERT INTO order_items (order_id, product_id, quantity, unit_price)
      VALUES (p_order_id, p_product_id, p_quantity, v_unit_price);
      UPDATE orders SET order_total = order_total + v_unit_price * p_quantity WHERE order_id = p_order_id;
    END add_order_item;
  
    FUNCTION get_order_total (p_order_id IN NUMBER) RETURN NUMBER IS
      v_order_total NUMBER;
    BEGIN
      SELECT order_total INTO v_order_total FROM orders WHERE order_id = p_order_id;
      RETURN v_order_total;
    END get_order_total;
  END sales_pkg;
  /
  ```

  ![](./img6/3.png)

### 4.数据库备份方案：

- 使用RMAN进行定期全备份和增量备份。

- 将备份文件存储在安全的远程位置。

- ```
  rman target /
  
  RUN {
    ALLOCATE CHANNEL disk1 DEVICE TYPE DISK FORMAT '/backup/%U';
    BACKUP AS BACKUPSET DATABASE PLUS ARCHIVELOG;
    RELEASE CHANNEL disk1;
  }
  ```

  

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|
