# 实验一

##   步骤一：权限分配 学号：202010111512 姓名：牟梦元



```sql
[oracle@oracle1 ~]$ sqlplus sys/123@localhost/pdborcl as sysdba

SQL*Plus: Release 12.2.0.1.0 Production on 星期一 5月 8 10:42:36 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.


连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> @$ORACLE_HOME/sqlplus/admin/plustrce.sql
SQL> 
SQL> drop role plustrace;

角色已删除。

SQL> create role plustrace;

角色已创建。

SQL> 
SQL> grant select on v_$sesstat to plustrace;

授权成功。

SQL> grant select on v_$statname to plustrace;

授权成功。

SQL> grant select on v_$mystat to plustrace;

授权成功。

SQL> grant plustrace to dba with admin option;

授权成功。

SQL> 
SQL> set echo off
SQL> create role plustrace;
create role plustrace

授权成功。

SQL> GRANT SELECT ON v_$sesstat TO plustrace;

授权成功。

SQL> GRANT SELECT ON v_$statname TO plustrace;

授权成功。

SQL> GRANT SELECT ON v_$mystat TO plustrace;

授权成功。

SQL> GRANT plustrace TO dba WITH ADMIN OPTION;

授权成功。

SQL> GRANT plustrace TO hr;

授权成功。

SQL> GRANT SELECT ON v_$sql TO hr;

授权成功。

SQL> GRANT SELECT ON v_$sql_plan TO hr;

授权成功。

SQL> GRANT SELECT ON v_$sql_plan_statistics_all TO hr;

授权成功。

SQL> GRANT SELECT ON v_$session TO hr;

授权成功。

SQL> GRANT SELECT ON v_$parameter TO hr; 

授权成功。
```





## 步骤二：查询部门信息

* 连接到用户

  ```sql
  $sqlplus hr/123@localhost/pdborcl
  ```




* 查询统计：

  ```sql
  SQL> set autotrace on
  SQL> SELECT d.department_name,count(e.job_id)as "部门总人数",
    2  avg(e.salary)as "平均工资"
    3  from hr.departments d,hr.employees e
    4  where d.department_id = e.department_id
    5  and d.department_name in ('IT','Sales')
    6  GROUP BY d.department_name;
  ```
  
  ![](./img/img1.png)
  
  ![](./img/img2.png)

## 查询二

```sql
SQL> SELECT d.department_name,count(e.job_id)as "部门总人数",
  2  avg(e.salary)as "平均工资"
  3  FROM hr.departments d,hr.employees e
  4  WHERE d.department_id = e.department_id
  5  GROUP BY d.department_name
  6  HAVING d.department_name in ('IT','Sales');
```

![](./img/img3.png)

![](./img/img4.png)
